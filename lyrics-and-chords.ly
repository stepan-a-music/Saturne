\version "2.16.2"

\header {
  title = "Saturne"
  instrument = "Guitare et chant"
  composer = "Georges Brassens"
  tagline = "stepan@ithaca.fr"
}

<<

  \relative c' {
    #(set-accidental-style 'default 'Voice)
    \autoBeamOff
    \key c \major
    \time 6/8
    \repeat volta 6 {
    r2 a8 c8 e4 e8 d8 f8 e8 d8 c4 (c8)
    c8 e8 a4 g8 f8 a8 g8 e4.(e8)
    e8 gis8 b8 gis8 b8 d4 c8 b8 a4
    a8 c8 a8 f4. g8 b8 g8 e4. (e8)
    e8 gis8 b8 gis4 b8 d4 c8 b8 a4(a4)
    a8 c8 a8 c8 b8 fis8 gis8 a4.(a8) r4}
  }

>>
